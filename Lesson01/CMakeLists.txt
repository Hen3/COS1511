﻿# CMakeList.txt : CMake project for Activity01, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

# Add source to this project's executable.
add_executable (Lesson01_A "ActivityA.cpp" )
add_executable (Lesson01_B "ActivityB.cpp" )
add_executable (Lesson02_C "ActivityC.cpp" )
add_executable (Lesson01_D "ActivityD.cpp" )
add_executable (Lesson01_1 "Exercise1.cpp" )

# TODO: Add tests and install targets if needed.
