#include <iostream>

int main()
{
	int start, stop;

	int afternoon = 0;
	int evening = 0;

	std::cout << "Wage calculation" << std::endl;
	std::cout << "================" << std::endl;
	std::cout << "Starting time: ";
	std::cin >> start;
	std::cout << "Finishing time: ";
	std::cin >> stop;

	if (start < 6)
	{
		if (stop <= 6)
		{
			afternoon = stop - start;
		}
		else
		{
			afternoon = 6 - start;
			evening = stop - 6;
		}
	}
	else
	{
		evening = stop - start;
	}

	float Payment = afternoon * 32.50f + evening * 44.0f;
	std::cout << std::endl << "The payment is R " << Payment << std::endl;

	return 0;
}