#include <iostream>

int main()
{
	float salary;
	std::cout << "Enter your yearly salary : ";
	std::cin >> salary;

	if (salary > 70000.0f)
	{
		std::cout << "Your payable income tax is : " << salary * 0.4 << std::endl;
	}
	else
	{
		std::cout << "Your payable income tax is : " << salary * 0.3 << std::endl;
	}
	return 0;
}