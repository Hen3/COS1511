#include <iostream>
#include <ios>
#include <iomanip>
int main()
{
	float Price;
	std::cout << "Enter old price : ";
	std::cin >> Price;

	float discount = 10.0f;
	if (Price > 200.0f)
	{
		std::cout << "Enter special discount : ";
		std::cin >> discount;
	}

	std::cout << std::setprecision(2) << std::fixed << std::endl;
	std::cout << "=============" << std::endl;
	std::cout << "Was: R" << Price << std::endl;
	std::cout << std::defaultfloat;
	std::cout << "Discount: " << discount << "%" << std::endl;
	discount /= 100.0f;
	std::cout << std::fixed;
	std::cout << "Now: R" << (1.0f - discount) * Price << std::endl;
	std::cout << "=============" << std::endl;
	return 0;
}