#include <iostream>
int main()
{
	std::cout << "Enter an upper case letter : ";
	char c;
	std::cin >> c;
	char of = c - 'A';
	int index = of + 1;
	std::cout << c << " is in position " << index << " in the alphabet." << std::endl;

	return 0;
}