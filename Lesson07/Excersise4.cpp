#include <iostream>
#include <string>

std::string Prompt(const std::string& p)
{
	std::string res;
	std::cout << p;
	std::cin >> res;
	return res;
}

int main()
{
	std::string P1 = Prompt("Enter a persons name : ");
	std::string P2 = Prompt("Enter another persons name : ");
	std::string col = Prompt("Enter a colour : ");
	float n;
	std::cout << "Enter a number : ";
	std::cin >> n;
	std::string noun = Prompt("Enter a noun : ");
	std::string adj = Prompt("Enter an adjective : ");

	std::cout << std::endl;
	std::cout << "Dialogue" << std::endl;
	std::cout << "========" << std::endl;
	std::cout << P1 << ":\tCouldn't you see that the traffic light was " << col << " ?" << std::endl;
	std::cout << P2 << ":\tBut I had " << n << " people and a " << noun << " in the car with me." << std::endl;
	std::cout << P1 << ":\tThat is so " << adj << "! You could have had them all killed." << std::endl;

	return 0;
}