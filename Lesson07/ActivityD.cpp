#include <iostream>
#include <ios>
#include <iomanip>

int main()
{
	float km, l, kml, l100km;
	std::cout << "Enter the distance driven in km : ";
	std::cin >> km;
	std::cout << "Enter the amount of fuel used in litre : ";
	std::cin >> l;
	kml = km / l;
	l100km = 100.0f / kml;

	std::cout << std::setw(8) << "kms" << std::setw(8) << "litres" << std::setw(8) << "km/l" << std::setw(8) << "l/100km" << std::endl;
	std::cout << std::setw(8) << km << std::setw(8) << l << std::setw(8) << kml << std::setw(8) << l100km << std::endl;

	return 0;
}