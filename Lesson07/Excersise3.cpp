#include <iostream>
#include <string>

int main()
{
	std::string word1, word2;
	std::cout << "Enter two words : ";
	std::cin >> word1 >> word2;
	char temp = word1[0];
	word1[0] = word2[0];
	word2[0] = temp;
	std::cout << word1 << " " << word2 << std::endl;
	return 0;
}