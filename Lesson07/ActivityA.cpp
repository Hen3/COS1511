#include <iostream>
#include <string>

int main()
{
	std::string firstname, lastname, title;
	std::cout << "Enter your full name in the form '<title> <first name> <last name>' : ";
	std::cin >> title >> firstname >> lastname;

	std::cout << lastname << ", " << title << ", " << firstname << std::endl;
	return 0;
}