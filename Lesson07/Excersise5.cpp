#include <iostream>
#include <string>

int main()
{
	long long n;
	std::string msg;
	std::cout << "Computer punishment" << std::endl;
	std::cout << "===================" << std::endl << std::endl;

	std::cout << "Repetitions ? ";
	std::cin >> n;

	std::cout << "Message ? ";
	std::cin.get();
	std::getline(std::cin, msg, '\n');

	std::cout << std::endl;

	while (n--)
	{
		std::cout << msg << std::endl;
	}

	return 0;
}