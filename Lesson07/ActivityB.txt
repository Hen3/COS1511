Simple answer : 
	Characters are ordinal, meaning as in integers there is a natural order in characters. ('A' comes before 'B' etc.)

Complex answer :
	The caracter type in c++ (and most other programming languages) is just an integer index into a picture array. 
	Incrementing the index gets the next picture. 
	The picture array is composed in such a manner that ordinal character subsets is given contiguous regions in the array.