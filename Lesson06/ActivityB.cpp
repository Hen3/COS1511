#include <iostream>

int main()
{
	int Mark1, Mark2, Mark3;
	float Average;

	std::cout << "Enter 3 marks : ";
	std::cin >> Mark1 >> Mark2 >> Mark3;

	Average = (Mark1 + Mark2 + Mark3) / 3.0f;

	std::cout << "Your average mark is : " << Average << std::endl;
}