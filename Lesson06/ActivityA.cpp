#include <iostream>

int main()
{
	float Item1, Item2, Item3, SubTotal, Vat, Total;

	std::cout << "Item 1 price : ";
	std::cin >> Item1;
	std::cout << "Item 2 price : ";
	std::cin >> Item2;
	std::cout << "Item 3 price : ";
	std::cin >> Item3;

	SubTotal = Item1 + Item2 + Item3;
	Vat = 0.14 * SubTotal;
	Total = SubTotal + Vat;
	std::cout << "------------------------------\n";
	std::cout << "Item 1\t\t| R " << Item1 << std::endl;
	std::cout << "Item 2\t\t| R " << Item2 << std::endl;
	std::cout << "Item 3\t\t| R " << Item3 << std::endl;
	std::cout << "------------------------------\n";
	std::cout << "Sub Total\t| R " << SubTotal << std::endl;
	std::cout << "Vat\t\t| R " << Vat << std::endl;
	std::cout << "==============================\n";
	std::cout << "Total\t\t| R " << Total << std::endl;

}