#include <iostream>
#include <iomanip>

int main()
{
	float length, width;
	std::cout << "Enter the length and width of a room in meters : ";
	std::cin >> length >> width;

	float Area = length * width;

	std::cout << std::setprecision(3) << std::fixed;

	std::cout << "The area of the room is " << Area << " m^2\n";
}