#include <iostream>

int main()
{
	std::cout << "Enter the temperature in Fahrenheit : ";
	float F;
	std::cin >> F;
	float C = 5.0 * (F - 32.0) / 9.0;
	const char Deg[] = { 32, 248, 67, 0 };

	std::cout << F << " Fahrenheit is equal to " << C << Deg << std::endl;
	return 0;
}