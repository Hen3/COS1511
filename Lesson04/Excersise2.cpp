#include <iostream>

int main()
{
	std::cout << "Enter the number of items : ";
	int Items;
	std::cin >> Items;
	std::cout << "Enter the number of item which fit in a box : ";
	int BoxCapacity;
	std::cin >> BoxCapacity;

	int Boxes = Items / BoxCapacity;
	int ExtraItems = Items % BoxCapacity;

	std::cout << "Sam needs " << Boxes << " to pack all but " << ExtraItems << " items.\n";
}