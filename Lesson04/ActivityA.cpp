#include <iostream>

int main()
{
	int time_1_minutes, time_1_seconds;
	int time_2_minutes, time_2_seconds;

	std::cout << "Enter a period of time : ";
	std::cin >> time_1_minutes >> time_1_seconds;

	std::cout << "Enter a second period of time : ";
	std::cin >> time_2_minutes >> time_2_seconds;

	int total_minutes, total_seconds;
	total_seconds = time_1_seconds + time_2_seconds;
	total_minutes = total_seconds / 60;
	total_seconds = total_seconds % 60;
	total_minutes += time_1_minutes + time_2_minutes;

	std::cout << "The total time is " << total_minutes << " minutes and " << total_seconds << " seconds.\n";
}