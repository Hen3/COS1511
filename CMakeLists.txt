﻿# CMakeList.txt : Top-level CMake project file, do global configuration
# and include sub-projects here.
#
cmake_minimum_required (VERSION 3.8)

project ("COS1511")

# Include sub-projects.
add_subdirectory ("Lesson01")
add_subdirectory ("Lesson02")
add_subdirectory ("Lesson03")
add_subdirectory ("Lesson04")
add_subdirectory ("Lesson06")
add_subdirectory ("Lesson07")
add_subdirectory ("Lesson08")
add_subdirectory ("Lesson09")
add_subdirectory ("Lesson10")
add_subdirectory ("Lesson11")
add_subdirectory ("Lesson12")
