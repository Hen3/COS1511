#include <iostream>

int main()
{
	int number;
	bool correct;
	do
	{
		std::cout << "Enter a number between 10 and 20, inclusive : ";
		std::cin >> number;

		if (number > 20)
		{
			std::cout << number << " is too large." << std::endl;
			correct = false;
		}
		else if (number < 10)
		{
			std::cout << number << " is too small." << std::endl;
			correct = false;
		}
		else
		{
			correct = true;
		}
	} while (!correct);

	std::cout << "The chosen number is " << number << std::endl;

	return 0;
}