#include <iostream>

int main()
{
	int n1, n2;
	std::cout << "Enter two numbers : ";
	std::cin >> n1 >> n2;
	if (n2 % n1)
	{
		std::cout << n1 << " is not a factor of " << n2 << std::endl;
	}
	else
	{
		std::cout << n1 << " is a factor of " << n2 << std::endl;
	}
	return 0;
}