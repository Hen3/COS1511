#include <iostream>

int main()
{
	std::cout << "Enter two numbers : ";
	int numerator, denominator;
	std::cin >> numerator >> denominator;
	std::cout << "The remainder of " << numerator << " divided by " << denominator << " is " << numerator % denominator << std::endl;
	return 0;

}