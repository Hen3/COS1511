cmake_minimum_required (VERSION 3.8)

add_executable(Lesson03_A "ActivityA.cpp")
add_executable(Lesson03_B "ActivityB.cpp")
add_executable(Lesson03_1 "Excersise1.cpp")
add_executable(Lesson03_3 "Excersise3.cpp")