#include <iostream>

int main()
{
	const float VAT = 1.14;
	float Price, Total = 0.0f;
	std::cout << "Enter the prices all items : " << std::endl;
	do
	{
		std::cin >> Price;
		Total += Price;
	} while (Price);

	std::cout << "The total price is R" << VAT * Total << std::endl;
	return 0;
}