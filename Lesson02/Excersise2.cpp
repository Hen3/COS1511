#include <iostream>


int main()
{
	std::cout << "There are 60 seconds in a minute." << std::endl;
	std::cout << "There are " << 60 * 60 << " seconds in an hour." << std::endl;
	std::cout << "There are " << 60 * 60 * 24 << " seconds an a day." << std::endl;
	std::cout << "There are " << 60 * 60 * 24 * 365 << " seconds in a year." << std::endl;
	return 0;
}