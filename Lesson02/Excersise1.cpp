﻿#include <iostream>

int main()
{
	std::cout << "80 / 5 + 70 / 6 <-> (80 / 5) + (70 / 6)" << std::endl;
	std::cout << "-5 + -4 - -3 <-> (-5 + -4) - -3" << std::endl;
	std::cout << "6 * 7 / 8 * 9 <-> (6 * 7) / 8 ) * 9" << std::endl;
	std::cout << "1 - 2 + 3 / 4 * 5 <-> (1 - 2) + ((3 / 4) * 5)" << std::endl;
	std::cout << "-1 + 23 / -4 + 56 <-> -1 + (23 / -4) + 56" << std::endl;
	return 0;
}