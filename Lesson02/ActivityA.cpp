#include <iostream>

void subactivity1()
{
	std::cout << 1 + 2 + 3 + 4 << std::endl;
}

void subactivity2()
{
	std::cout << "The product of 371 x 194 is " << 371 * 194 << std::endl;
}

int main()
{
	subactivity1();
	subactivity2();
	std::cout << "The product of 1 to 5 is " << 1 * 2 * 3 * 4 * 5 << std::endl;
	return 0;
}
