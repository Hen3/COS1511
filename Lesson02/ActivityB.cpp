#include <iostream>

int main()
{
	std::cout << "i)\t\t12 * 23 + 34 * 45\t\t= " << 12 * 23 + 34 * 45 << std::endl;
	std::cout << "ii)\t\t4 * (5 + 6)\t\t\t= " << 4 * (5 + 6) << std::endl;
	std::cout << "iii)\t\t543 - 234\t\t\t= " << 543 - 234 << std::endl;
	std::cout << "iv)\t\t234 - 543\t\t\t= " << 234 - 543 << std::endl;
	std::cout << "v)\t\t30 / 3\t\t\t\t= " << 30 / 3 << std::endl;
	std::cout << "vi)\t\t20 / 3\t\t\t\t= " << 20 / 3 << std::endl;
	std::cout << "vii)\t\t10 / 3\t\t\t\t= " << 10 / 3 << std::endl;
	std::cout << "viii)\t\t(357 + 468) / (-19 * 28)\t= " << (357 + 468) / (-19 * 28) << std::endl;
	return 0;
}